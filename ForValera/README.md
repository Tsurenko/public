# For Valera

## Server

A tiny golang application which listens to http://application:8090 and prints all HTTP request-headers into STDOUT for demo purposes.

## Forwarder

[Caddy2](https://caddyserver.com)-based container which listens to http://localhost:8090, adds 2 custom headers: `token` and `Accept-Language` and forwards to http://application:8090

## Configuration

Replace/add custom headers to Caddyfile `header_upstream` section (1 line per header)


## Running

```
docker-compose build
docker-compose up

Open http://localhost:8090 in browser and check that specified headers have reached application and are present in both web and STDOUT
```